const pkg = require('../package.json');
const schema = require('./schema.json');

module.exports = {
  schema,
  version: {
    latest: () => pkg.version
  }
};
